package ru.t1.ktubaltseva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(@NotNull final String yaml) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(yaml, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @NotNull final byte[] bytes = yaml.getBytes();
        @NotNull final String filename = table + ".yaml";
        @NotNull final File file = new File(filename);
        if (!file.exists()) file.createNewFile();
        @NotNull final Path path = Files.write(Paths.get(filename), bytes, StandardOpenOption.APPEND);
    }

}
