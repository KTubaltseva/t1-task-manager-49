package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.service.dto.ProjectDTOService;
import ru.t1.ktubaltseva.tm.service.dto.UserDTOService;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDTOService service = new ProjectDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.remove(USER_1);
        userService.remove(USER_2);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear(USER_1.getId());
        service.clear(USER_2.getId());
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_PROJECT));

        @Nullable final ProjectDTO projectToAdd = USER_1_PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final ProjectDTO projectAdded = service.add(projectToAdd);
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd.getId(), projectAdded.getId());

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    public void addByUserId() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(USER_1.getId(), NULL_PROJECT));
        Assert.assertThrows(AuthRequiredException.class, () -> service.add(NULL_USER_ID, USER_1_PROJECT_1));

        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final ProjectDTO projectToAddByUser = USER_1_PROJECT_1;
        @Nullable final String projectToAddByUserId = projectToAddByUser.getId();

        @Nullable final ProjectDTO projectAddedByUser = service.add(userToAddId, projectToAddByUser);
        Assert.assertNotNull(projectAddedByUser);
        Assert.assertTrue(service.existsById(projectToAddByUserId));

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(projectToAddByUserId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneById.getId());

        @Nullable final ProjectDTO projectFindOneByIdByUserIdToAdd = service.findOneById(userToAddId, projectToAddByUserId);
        Assert.assertNotNull(projectFindOneByIdByUserIdToAdd);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneByIdByUserIdToAdd.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userNoAddId, projectToAddByUserId));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToAddId, NON_EXISTENT_PROJECT_ID));
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<ProjectDTO> projectList = service.add(PROJECT_LIST);
        Assert.assertNotNull(projectList);
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @Nullable final ProjectDTO projectFindOneById = service.findOneById(project.getId());
            Assert.assertEquals(project.getId(), projectFindOneById.getId());
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_PROJECT_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_PROJECT_ID));

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    public void findOneByIdByUserId() throws AbstractException {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;
        @NotNull final UserDTO userExists = USER_1;
        service.add(userExists.getId(), projectExists);

        Assert.assertThrows(AuthRequiredException.class, () -> service.findOneById(NULL_USER_ID, projectExists.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userExists.getId(), NULL_PROJECT_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userExists.getId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        service.add(projectExists);
        @NotNull final Collection<ProjectDTO> projectsFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    public void findAllByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;
        @NotNull final UserDTO userExists = USER_1;
        service.add(userExists.getId(), projectExists);

        @NotNull final Collection<ProjectDTO> projectsFindAllByUserRepNoEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);

        @NotNull final Collection<ProjectDTO> projectsFindAllByNonExistentUser = service.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(projectsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), projectsFindAllByNonExistentUser);
    }

    @Test
    public void clear() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.clear(NULL_USER_ID));

        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<ProjectDTO> projectByUserToClearList = service.add(USER_1_PROJECT_LIST);
        @NotNull final Collection<ProjectDTO> projectByUserNoClearList = service.add(USER_2_PROJECT_LIST);
        service.clear(userToClearId);

        for (@NotNull final ProjectDTO projectByUserToClear : projectByUserToClearList) {
            Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectByUserToClear.getId()));
        }
        Assert.assertEquals(0, service.findAll(userToClearId).size());

        for (@NotNull final ProjectDTO projectByUserNoClear : projectByUserNoClearList) {
            @Nullable final ProjectDTO projectFindOneById = service.findOneById(projectByUserNoClear.getId());
            Assert.assertEquals(projectByUserNoClear.getId(), projectFindOneById.getId());
        }
        Assert.assertNotEquals(0, service.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final ProjectDTO projectToRemove = USER_1_PROJECT_1;
        service.add((projectToRemove));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(NULL_PROJECT));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(NON_EXISTENT_PROJECT));

        service.remove(projectToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectToRemove.getId()));
    }

    @Test
    public void removeOneByUserId() throws AbstractException {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final ProjectDTO projectByUserToRemove = service.add((USER_1_PROJECT_1));
        @Nullable final ProjectDTO projectByUserNoRemove = service.add((USER_2_PROJECT_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(NON_EXISTENT_USER_ID, projectByUserToRemove));
        Assert.assertThrows(AuthRequiredException.class, () -> service.remove(NULL_USER_ID, projectByUserToRemove));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(userToRemoveId, NULL_PROJECT));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(userToRemoveId, NON_EXISTENT_PROJECT));

        service.remove(userToRemoveId, projectByUserToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(userToRemoveId, projectByUserNoRemove));

        @Nullable final ProjectDTO projectNoRemovedFindOneById = service.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final ProjectDTO projectToRemove = USER_1_PROJECT_1;
        service.add((projectToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_PROJECT_ID));

        service.removeById(projectToRemove.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectToRemove.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractException {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final ProjectDTO projectByUserToRemove = service.add((USER_1_PROJECT_1));
        @Nullable final ProjectDTO projectByUserNoRemove = service.add((USER_2_PROJECT_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID, projectByUserToRemove.getId()));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeById(NULL_USER_ID, projectByUserToRemove.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userToRemoveId, NULL_PROJECT_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, NON_EXISTENT_PROJECT_ID));

        service.removeById(userToRemoveId, projectByUserToRemove.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, projectByUserNoRemove.getId()));

        @Nullable final ProjectDTO projectNoRemovedFindOneById = service.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_PROJECT_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_PROJECT_ID));
        Assert.assertTrue(service.existsById(projectExists.getId()));
    }

    @Test
    public void createName() throws Exception {
        @NotNull final UserDTO existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, PROJECT_NAME));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME));

        @NotNull final ProjectDTO createdProject = service.create(existentUser.getId(), PROJECT_NAME);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertTrue(service.existsById(createdProject.getId()));

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

    @Test
    public void createNameDesc() throws Exception {
        @NotNull final UserDTO existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME, PROJECT_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(existentUser.getId(), PROJECT_NAME, NULL_DESC));

        @NotNull final ProjectDTO createdProject = service.create(existentUser.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(PROJECT_DESC, createdProject.getDescription());
        Assert.assertTrue(service.existsById(createdProject.getId()));

        @Nullable final ProjectDTO projectFindOneById = service.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        @NotNull final UserDTO userNoUpdate = USER_2;
        @Nullable final ProjectDTO projectToUpdate = service.add((USER_1_PROJECT_1));
        @Nullable final ProjectDTO projectNoUpdate = service.add((USER_1_PROJECT_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.changeStatusById(NULL_USER_ID, projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.changeStatusById(NON_EXISTENT_USER_ID, projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.changeStatusById(userNoUpdate.getId(), projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeStatusById(userToUpdate.getId(), NULL_PROJECT_ID, PROJECT_STATUS));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.changeStatusById(userToUpdate.getId(), NON_EXISTENT_PROJECT_ID, PROJECT_STATUS));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeStatusById(userToUpdate.getId(), projectToUpdate.getId(), NULL_STATUS));

        @Nullable final ProjectDTO projectUpdated = service.changeStatusById(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_STATUS);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_STATUS, projectUpdated.getStatus());

        @Nullable final ProjectDTO projectFindOneByIdToUpdate = service.findOneById(projectToUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdToUpdate);
        Assert.assertEquals(projectFindOneByIdToUpdate.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_STATUS, projectFindOneByIdToUpdate.getStatus());

        @Nullable final ProjectDTO projectFindOneByIdNoUpdate = service.findOneById(projectNoUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdNoUpdate);
        Assert.assertEquals(projectFindOneByIdNoUpdate.getId(), projectNoUpdate.getId());
        Assert.assertNotEquals(PROJECT_STATUS, projectFindOneByIdNoUpdate.getStatus());
    }

    @Test
    public void updateProjectById() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        @NotNull final UserDTO userNoUpdate = USER_2;
        @Nullable final ProjectDTO projectToUpdate = service.add((USER_1_PROJECT_1));
        @Nullable final ProjectDTO projectNoUpdate = service.add((USER_1_PROJECT_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.update(NULL_USER_ID, projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_USER_ID, projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(userNoUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(IdEmptyException.class, () -> service.update(userToUpdate.getId(), NULL_PROJECT_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(userToUpdate.getId(), NON_EXISTENT_PROJECT_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.update(userToUpdate.getId(), projectToUpdate.getId(), NULL_NAME, PROJECT_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.update(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, NULL_DESC));

        @Nullable final ProjectDTO projectUpdated = service.update(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_NAME, projectUpdated.getName());
        Assert.assertEquals(PROJECT_DESC, projectUpdated.getDescription());

        @Nullable final ProjectDTO projectFindOneByIdToUpdate = service.findOneById(projectToUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdToUpdate);
        Assert.assertEquals(projectFindOneByIdToUpdate.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_NAME, projectFindOneByIdToUpdate.getName());
        Assert.assertEquals(PROJECT_DESC, projectFindOneByIdToUpdate.getDescription());

        @Nullable final ProjectDTO projectFindOneByIdNoUpdate = service.findOneById(projectNoUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdNoUpdate);
        Assert.assertEquals(projectFindOneByIdNoUpdate.getId(), projectNoUpdate.getId());
        Assert.assertNotEquals(PROJECT_NAME, projectFindOneByIdNoUpdate.getName());
        Assert.assertNotEquals(PROJECT_DESC, projectFindOneByIdNoUpdate.getDescription());
    }

}
