package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.service.dto.UserDTOService;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDTOService service = new UserDTOService(connectionService, propertyService);

    @Before
    public void before() {
    }

    @After
    @SneakyThrows
    public void after() {
        try {
            service.remove(USER_1);
        } catch (AbstractException e) {

        }
        try {
            service.remove(USER_2);
        } catch (AbstractException e) {

        }
        try {
            service.removeByLogin(USER_LOGIN);
        } catch (AbstractException e) {

        }
        try {
            service.remove(USER_WITH_LOGIN_EMAIL);
        } catch (AbstractException e) {

        }
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_USER));

        @Nullable final UserDTO userToAdd = USER_1;
        @Nullable final String userToAddId = userToAdd.getId();

        @Nullable final UserDTO userAdded = service.add(userToAdd);
        Assert.assertNotNull(userAdded);
        Assert.assertEquals(userToAdd.getId(), userAdded.getId());

        @Nullable final UserDTO userFindOneById = service.findOneById(userToAddId);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userToAdd.getId(), userFindOneById.getId());
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<UserDTO> userList = service.add(USER_LIST);
        Assert.assertNotNull(userList);
        for (@NotNull final UserDTO user : USER_LIST) {
            @Nullable final UserDTO userFindOneById = service.findOneById(user.getId());
            Assert.assertEquals(user.getId(), userFindOneById.getId());
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        service.add(userExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID));

        @Nullable final UserDTO userFindOneById = service.findOneById(userExists.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists.getId(), userFindOneById.getId());
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        userExists.setLogin(USER_LOGIN);
        service.add(userExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findByLogin(NON_EXISTENT_USER_LOGIN));

        @Nullable final UserDTO userFindOneById = service.findByLogin(USER_LOGIN);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists.getId(), userFindOneById.getId());
    }

    @Test
    public void findByEmail() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        userExists.setEmail(USER_EMAIL);
        service.add(userExists);

        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(NULL_EMAIL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findByEmail(NON_EXISTENT_USER_EMAIL));

        @Nullable final UserDTO userFindOneById = service.findByEmail(USER_EMAIL);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists.getId(), userFindOneById.getId());
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;

        service.add(userExists);
        @NotNull final Collection<UserDTO> usersFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(usersFindAllNoEmpty);
    }

    @Test
    public void createLoginPassword() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD));

        @NotNull final UserDTO createdUser = service.create(USER_LOGIN, USER_PASSWORD);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final UserDTO userFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(createdUser.getId(), userFindOneById.getId());
    }

    @Test
    public void createLoginPasswordEmail() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD, USER_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD, USER_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(USER_LOGIN, USER_PASSWORD, NULL_EMAIL));

        @NotNull final UserDTO createdUser = service.create(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertEquals(USER_EMAIL, createdUser.getEmail());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final UserDTO userFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(createdUser.getId(), userFindOneById.getId());
    }

    @Test
    public void createLoginPasswordRole() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD, USER_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD, USER_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> service.create(USER_LOGIN, USER_PASSWORD, NULL_ROLE));

        @NotNull final UserDTO createdUser = service.create(USER_LOGIN, USER_PASSWORD, USER_ROLE);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertEquals(USER_ROLE, createdUser.getRole());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final UserDTO userFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(createdUser.getId(), userFindOneById.getId());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final UserDTO userToRemove = USER_1;
        service.add((userToRemove));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(NULL_USER));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.remove(NON_EXISTENT_USER));

        service.remove(userToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToRemove.getId()));
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final UserDTO userToRemove = USER_1;
        service.add((userToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID));

        service.removeById(userToRemove.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToRemove.getId()));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        @Nullable final UserDTO userToRemove = USER_1;
        userToRemove.setLogin(USER_LOGIN);
        service.add((userToRemove));

        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByLogin(NON_EXISTENT_USER_LOGIN));

        service.removeByLogin(USER_LOGIN);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToRemove.getId()));
    }

    @Test
    public void removeByEmail() throws AbstractException {
        @Nullable final UserDTO userToRemove = USER_1;
        userToRemove.setEmail(USER_EMAIL);
        service.add((userToRemove));

        Assert.assertThrows(EmailEmptyException.class, () -> service.removeByEmail(NULL_EMAIL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByEmail(NON_EXISTENT_USER_EMAIL));

        service.removeByEmail(USER_EMAIL);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToRemove.getId()));
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        service.add(userExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_USER_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_USER_ID));
        Assert.assertTrue(service.existsById(userExists.getId()));
    }

    @Test
    public void isLoginExists() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        userExists.setLogin(USER_LOGIN);
        service.add(userExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.isLoginExists(NULL_LOGIN));

        Assert.assertFalse(service.isLoginExists(NON_EXISTENT_USER_LOGIN));
        Assert.assertTrue(service.isLoginExists(USER_LOGIN));
    }

    @Test
    public void isEmailExists() throws AbstractException {
        @NotNull final UserDTO userExists = USER_1;
        userExists.setLogin(USER_EMAIL);
        service.add(userExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.isLoginExists(NULL_EMAIL));

        Assert.assertFalse(service.isLoginExists(NON_EXISTENT_USER_EMAIL));
        Assert.assertTrue(service.isLoginExists(USER_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        userToUpdate.setLocked(false);
        service.add(userToUpdate);
        @NotNull final UserDTO userNoUpdate = USER_2;
        userNoUpdate.setLocked(false);
        service.add(userNoUpdate);

        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(NON_EXISTENT_USER_LOGIN));

        service.lockUserByLogin(userToUpdate.getLogin());

        @Nullable final UserDTO userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate.getId(), userToUpdate.getId());
        Assert.assertTrue(userFindOneByIdToUpdate.isLocked());

        @Nullable final UserDTO userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate.getId(), userNoUpdate.getId());
        Assert.assertFalse(userFindOneByIdNoUpdate.isLocked());
    }

    @Test
    public void unlockUserByLogin() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        userToUpdate.setLocked(true);
        service.add(userToUpdate);
        @NotNull final UserDTO userNoUpdate = USER_2;
        userNoUpdate.setLocked(true);
        service.add(userNoUpdate);

        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.unlockUserByLogin(NON_EXISTENT_USER_LOGIN));

        service.unlockUserByLogin(userToUpdate.getLogin());

        @Nullable final UserDTO userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate.getId(), userToUpdate.getId());
        Assert.assertFalse(userFindOneByIdToUpdate.isLocked());

        @Nullable final UserDTO userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate.getId(), userNoUpdate.getId());
        Assert.assertTrue(userFindOneByIdNoUpdate.isLocked());
    }

    @Test
    public void setPassword() throws Exception {
        @NotNull final UserDTO userToUpdate = service.add(USER_1);
        @NotNull final UserDTO userNoUpdate = service.add(USER_2);

        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(NULL_USER_ID, USER_PASSWORD));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.setPassword(NON_EXISTENT_USER_ID, USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(userToUpdate.getId(), NULL_PASSWORD));

        @Nullable final UserDTO userUpdated = service.setPassword(userToUpdate.getId(), USER_PASSWORD);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated.getId(), userToUpdate.getId());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), userUpdated.getPasswordHash());

        @Nullable final UserDTO userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate.getId(), userToUpdate.getId());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), userFindOneByIdToUpdate.getPasswordHash());

        @Nullable final UserDTO userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate.getId(), userNoUpdate.getId());
        Assert.assertNotEquals(HashUtil.salt(propertyService, USER_PASSWORD), userFindOneByIdNoUpdate.getPasswordHash());
    }

    @Test
    public void updateUser() throws AbstractException {
        @NotNull final UserDTO userToUpdate = service.add(USER_1);
        @NotNull final UserDTO userNoUpdate = service.add(USER_2);

        Assert.assertThrows(IdEmptyException.class, () -> service.update(NULL_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME));
        service.update(userToUpdate.getId(), NULL_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        service.update(userToUpdate.getId(), USER_FIRST_NAME, NULL_MIDDLE_NAME, USER_LAST_NAME);
        service.update(userToUpdate.getId(), USER_FIRST_NAME, USER_MIDDLE_NAME, NULL_LAST_NAME);

        @Nullable final UserDTO userUpdated = service.update(userToUpdate.getId(), USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated.getId(), userToUpdate.getId());
        Assert.assertEquals(USER_FIRST_NAME, userUpdated.getFirstName());
        Assert.assertEquals(USER_MIDDLE_NAME, userUpdated.getMiddleName());
        Assert.assertEquals(USER_LAST_NAME, userUpdated.getLastName());

        @Nullable final UserDTO userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate.getId(), userToUpdate.getId());
        Assert.assertEquals(USER_FIRST_NAME, userFindOneByIdToUpdate.getFirstName());
        Assert.assertEquals(USER_MIDDLE_NAME, userFindOneByIdToUpdate.getMiddleName());
        Assert.assertEquals(USER_LAST_NAME, userFindOneByIdToUpdate.getLastName());

        @Nullable final UserDTO userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate.getId(), userNoUpdate.getId());
        Assert.assertNotEquals(USER_FIRST_NAME, userFindOneByIdNoUpdate.getFirstName());
        Assert.assertNotEquals(USER_MIDDLE_NAME, userFindOneByIdNoUpdate.getMiddleName());
        Assert.assertNotEquals(USER_LAST_NAME, userFindOneByIdNoUpdate.getLastName());
    }

}
