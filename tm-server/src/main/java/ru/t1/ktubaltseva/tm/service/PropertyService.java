package ru.t1.ktubaltseva.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "3535";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "8800555";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String GIT_BRANCH_KEY = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID_KEY = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME_KEY = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL_KEY = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE_KEY = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME_KEY = "gitCommitTime";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "3600";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "123";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://127.0.0.1:5432/postgres";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_KEY = "database.hbm2ddl";

    @NotNull
    private static final String DATABASE_HBM2DDL_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.showSql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE_KEY = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS_KEY = "database.factory_class";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_KEY = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_KEY = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_KEY = "database.region_prefix";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_KEY = "database.config_file_path";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String DATABASE_INIT_TOKEN_KEY = "database.init_token";

    @NotNull
    private static final String DATABASE_INIT_TOKEN_DEFAULT = "false";


    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    public String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public String getGitBranch() {
        return read(GIT_BRANCH_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME_KEY);
    }

    @Override
    public @NotNull Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @Override
    public @NotNull String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    public Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    public String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    public String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseURL() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseHBM2DDL() {
        return getStringValue(DATABASE_HBM2DDL_KEY, DATABASE_HBM2DDL_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseSecondLvlCache() {
        return getStringValue(DATABASE_SECOND_LVL_CACHE_KEY, DATABASE_SECOND_LVL_CACHE_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS_KEY, DATABASE_FACTORY_CLASS_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE_KEY, DATABASE_USE_QUERY_CACHE_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS_KEY, DATABASE_USE_MIN_PUTS_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX_KEY, DATABASE_REGION_PREFIX_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH_KEY, DATABASE_CONFIG_FILE_PATH_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseInitToken() {
        return getStringValue(DATABASE_INIT_TOKEN_KEY, DATABASE_INIT_TOKEN_DEFAULT);
    }

}
