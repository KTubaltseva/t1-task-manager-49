package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedWBSDTOService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

}
