package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;

public interface IAuthService {

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

    @NotNull
    UserDTO registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    SessionDTO validateToken(
            @Nullable final String token
    );

    void invalidate(
            @Nullable final SessionDTO session
    ) throws EntityNotFoundException, IdEmptyException, AbstractException;

}
