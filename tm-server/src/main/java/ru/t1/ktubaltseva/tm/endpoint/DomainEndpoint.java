package ru.t1.ktubaltseva.tm.endpoint;

import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktubaltseva.tm.api.service.IServiceLocator;
import ru.t1.ktubaltseva.tm.dto.request.data.*;
import ru.t1.ktubaltseva.tm.dto.response.data.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AccessDeniedException;
import ru.t1.ktubaltseva.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.REQUEST;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.ktubaltseva.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBackupResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBackupRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataLoadBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBase64Request request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBinaryRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataLoadJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataLoadXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadYamlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBackupResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBackupRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataSaveBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBase64Request request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBinaryRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataSaveJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataSaveXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveYamlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }

    @Override
    @WebMethod
    public void initScheme(
            @WebParam(name = "initToken") @Nullable final String initToken
    ) throws AccessDeniedException, LiquibaseException {
        @NotNull final String token = getPropertyService().getDatabaseInitToken();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        getDomainService().initScheme();
    }

    @Override
    @WebMethod
    public void dropScheme(
            @WebParam(name = "session") @Nullable final Session session
    ) throws DatabaseException {
        getDomainService().dropScheme();
    }

}
