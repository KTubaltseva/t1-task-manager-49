package ru.t1.ktubaltseva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.api.service.dto.*;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.endpoint.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.service.dto.*;
import ru.t1.ktubaltseva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.ktubaltseva.tm.command";

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService);

    @Getter
    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(
            connectionService,
            propertyService
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (IOException e) {
            loggerService.error(e);
        }
    }

    private void initDemoData() throws Exception {
        if (userService.getSize() == 0) {
            @NotNull final UserDTO user1 = userService.create("USER1", "USER1");
            @NotNull final UserDTO user2 = userService.create("USER2", "USER2");
            @NotNull final UserDTO admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

            projectService.create(user1.getId(), "pn2", "pd2");
            projectService.create(user1.getId(), "pn5", "pd5");
            projectService.create(user2.getId(), "pn3", "pd3");
            projectService.create(admin.getId(), "pn4", "pd4");

            taskService.create(user1.getId(), "tn2", "td2");
            taskService.create(user1.getId(), "tn5", "td5");
            taskService.create(user2.getId(), "tn3", "td3");
            taskService.create(admin.getId(), "tn4", "td4");
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final Integer port = getPropertyService().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() throws Exception {
        initPID();
        initDemoData();
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    @SneakyThrows
    private void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

}
