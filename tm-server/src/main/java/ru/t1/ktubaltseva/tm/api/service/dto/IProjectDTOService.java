package ru.t1.ktubaltseva.tm.api.service.dto;

import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

public interface IProjectDTOService extends IUserOwnedWBSDTOService<ProjectDTO> {

}
