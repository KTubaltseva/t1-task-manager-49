package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String DESC = "Display program version.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-v";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

}
