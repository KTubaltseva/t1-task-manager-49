package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.IHasCreated;
import ru.t1.ktubaltseva.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO implements IHasCreated {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "login", columnDefinition = "varchar(30)", nullable = false, unique = true)
    private String login = "";

    @NotNull
    @Column(name = "password_hash", columnDefinition = "varchar(36)", nullable = false)
    private String passwordHash = "";

    @Nullable
    @Column(name = "email", columnDefinition = "varchar(30)")
    private String email;

    @Nullable
    @Column(name = "first_name", columnDefinition = "varchar(30)")
    private String firstName;

    @Nullable
    @Column(name = "middle_name", columnDefinition = "varchar(30)")
    private String middleName;

    @Nullable
    @Column(name = "last_name", columnDefinition = "varchar(30)")
    private String lastName;

    @NotNull
    @Column(name = "role", columnDefinition = "varchar(30)", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked", columnDefinition = "varchar(30)", nullable = false)
    private boolean locked = false;

    public UserDTO(@NotNull final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += login;
        if (email != null)
            result += "\t" + email;
        result += "\t" + role;
        return result;
    }
}
