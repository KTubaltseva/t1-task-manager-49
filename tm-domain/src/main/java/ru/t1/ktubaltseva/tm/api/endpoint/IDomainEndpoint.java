package ru.t1.ktubaltseva.tm.api.endpoint;

import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.data.*;
import ru.t1.ktubaltseva.tm.dto.response.data.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AccessDeniedException;
import ru.t1.ktubaltseva.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IDomainEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataLoadBackupResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBackupRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBase64Request request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBinaryRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadJsonJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadXmlJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadYamlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveBackupResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBackupRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBase64Request request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBinaryRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveJsonJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveXmlJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveYamlFasterXmlRequest request
    ) throws AbstractException;

    @WebMethod
    void initScheme(
            @WebParam(name = "initToken") @Nullable String initToken
    ) throws AccessDeniedException, LiquibaseException;

    @WebMethod
    void dropScheme(
            @WebParam(name = "session") @Nullable Session session
    ) throws DatabaseException;

}

